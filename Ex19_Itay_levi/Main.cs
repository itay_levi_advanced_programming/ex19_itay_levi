﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ex19_Itay_levi
{
    public partial class Main : Form
    {
        private string path;
        private Dictionary<string, List<Event>> _events;

        public Main()
        {
            InitializeComponent();

            this.Hide();
            Form wnd = new Login(this);
            wnd.ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void mcBirthdays_DateChanged(object sender, DateRangeEventArgs e)
        {
            string date = mcBirthdays.SelectionRange.Start.ToShortDateString();
            if (_events.ContainsKey(date))
            {
                List<Event> eventList = _events[date];
                lblData.Text = "";
                for (int i = 0; i < eventList.Count; i++)
                {
                    lblData.Text += eventList[i].name + " has an " + eventList[i].detail + "\n";
                }
                
            }
            else
            {
                lblData.Text = "No Events in this day";
            }
        }

        private void InitData(string username)
        {
            path = Path.Combine(Environment.CurrentDirectory, username+"BD.txt").ToString();
            if (File.Exists(path))
            {
                StreamReader file = File.OpenText(path);
                String line;
                _events = new Dictionary<string, List<Event>>();
               
                while ((line = file.ReadLine()) != null)
                {
                    string[] dets = line.Split(',');
                    if (dets.Length >= 3)
                    {
                        if (_events.ContainsKey(dets[1]))
                        {
                            _events[dets[1]].Add(new Event(dets[0], dets[2]));
                        }
                        else
                        {
                            List<Event> eventList = new List<Event>();
                            eventList.Add(new Event(dets[0], dets[2]));
                            _events.Add(dets[1], eventList);
                        }
                    }
                }
                mcBirthdays_DateChanged(null, null);
                file.Close();
            }
            else
            {
                Application.Exit();
            }
        }

        public void Reshow(string username)
        {
            this.Show();
            _events = new Dictionary<string, List<Event>>();
            InitData(username);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (tbName.Text.Length != 0 && tbDetail.Text.Length != 0)
            {
                string date = dtpAdd.Value.ToShortDateString();
                Event ev = new Event(tbName.Text, tbDetail.Text);
                if (_events.ContainsKey(date))
                {
                    _events[date].Add(ev);
                }
                else
                {
                    List<Event> l = new List<Event>();
                    l.Add(ev);
                    _events.Add(date,l);
                }
                AddToFile(date, ev);
                tabs.SelectTab(0);
                mcBirthdays_DateChanged(null, null);
            }
        }

        private void AddToFile(string date, Event e)
        {
            using (StreamWriter file = File.AppendText(path))
            {
                file.WriteLine(String.Format("\n{0},{1},{2}", e.name, date, e.detail));
                file.Close();
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            string date = dtpRemove.Value.ToShortDateString();
            cmbOptions.Items.Clear();
            if (_events.ContainsKey(date))
            {
                cmbOptions.Items.AddRange(_events[date].ToArray());
            }
        }

        private void tabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = false;
            string date = dtpRemove.Value.ToShortDateString();
            if (_events.ContainsKey(date))
            {
                cmbOptions.Items.Clear();
                cmbOptions.Items.AddRange(_events[date].ToArray());
                if (_events[date].Count == 1)
                {
                    cmbOptions.SelectedIndex = 1;
                    btnRemove.Enabled = true;
                    cmbOptions.Enabled = false;
                }
                else
                {
                    cmbOptions.Text = "-select an event-";
                }

            }
            tbName.Text = tbDetail.Text = "";
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            string date = dtpRemove.Value.ToShortDateString();
            RemoveFromFile(date, _events[date][cmbOptions.SelectedIndex]);
            _events[date].RemoveAt(cmbOptions.SelectedIndex);
            mcBirthdays_DateChanged(null, null);
            cmbOptions.ResetText();
            cmbOptions.Items.Clear();
            btnRemove.Enabled = false;
            tabs.SelectedIndex = 0;
            
        }

        private void cmbOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = true;
        }


        private void RemoveFromFile(string date, Event e)
        {
            string tempFile = Path.GetTempFileName();

            using (var sr = new StreamReader(path))
            using (var sw = new StreamWriter(tempFile))
            {
                string line;
                string remove = String.Format("{0},{1},{2}", e.name, date, e.detail);
                while ((line = sr.ReadLine()) != null)
                {
                    if (line != remove)
                        sw.WriteLine(line);
                }
            }
            File.Delete(path);
            File.Move(tempFile, path);
        }
    }
}
