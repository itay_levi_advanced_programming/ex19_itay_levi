﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ex19_Itay_levi
{
    public partial class Login : Form
    {
        Dictionary<String, String> _data;
        Main _form;

        public Login(Main form)
        {
            InitializeComponent();

            InitData();
            this._form = form;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (_data.ContainsKey(tbUsername.Text)
                && _data[tbUsername.Text].Equals(tbPassword.Text))
            {
                _form.Reshow(tbUsername.Text);
                this.Close();
            } 
            else
            {
                Application.Exit();
            }
        }

        private void InitData()
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Users.txt").ToString();
            if (!File.Exists(path))
            {
                StreamWriter sw = File.CreateText(path);
                sw.Close();
            }
            StreamReader sr = File.OpenText(path);
            String s;
            _data = new Dictionary<string, string>();
            while ((s = sr.ReadLine()) != null)
            {
                _data.Add(s.Split(',')[0], s.Split(',')[1]);
            }
        }
    }
}
