﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex19_Itay_levi
{
    class Event
    {
        public string name { get; set;}

        public string detail {get; set; }

        public Event(string name, string e)
        {
            this.name = name;
            detail = e;
        }

        public override string ToString()
        {
            return name+", "+detail;
        }
    }
}
